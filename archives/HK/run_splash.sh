#!/bin/bash
# Disconnect jack
jack_disconnect mplayer:out_0 system:playback_1

# Clean any process running
killall -s SIGKILL splash
killall -s SIGKILL splash-scene

# Run Splash
cd /media/data/splash_data/HK
DISPLAY=:0.1 splash-scene GPU_1 &
splash -o config.json
killall mplayer
