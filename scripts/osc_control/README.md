### How to use

* Allows OSC messages to be sent to splash in order to change the images linked to objects, or move/rotate objects in their environment.

* If you want to control splash from your phone, install AndrOSC (free app in the PlayStore) on the Android device that is going to be used to control Splash.

* The possible OSC commands are:

```bash
# For rotating or moving an object
# The axis is x, y or z. You can move on 1, 2 or 3 axis at the same time.
# The angle/position will be $1 if using a slider on AndrOSC.
/splash/objectName rotate axis1 angle1 axis2 angle2
/splash/objectName move axis1 position1

# This also works with the alias of an object. As a bonus, if multiple objects
# share the same alias, they will all move!
/splash/alias rotate axis1 angle1 axis2 angle2
/splash/alias move axis1 position

# For linking an image to an object (will also unlink any other images linked to that object)
/splash link imageName objectName
```

* Make sure the Android device and the computer running splash are on the same LAN network. Then, in AndrOSC, open the Network Settings, change the IP address to the Local IP of the computer running splash and change the Port to 9000

* Launch Splash using osc_control.py

```bash
splash -P osc_control.py splash_configuration.json
```

* open splash.json in AndrOSC. The controls should work. If not, try rebooting AndrOSC.
